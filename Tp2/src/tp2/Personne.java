package tp2;

public class Personne {
	
	private String nom;
	private String prenom;
	
	public Personne(String n, String p){
		this.nom=n;
		this.prenom=p;
	}
	
	public String getn(){
		return this.nom;
	}
	
	public String getp(){
		return this.prenom;
	}
}
