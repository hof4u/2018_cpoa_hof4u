package tp2;
import java.util.HashMap;

public class AnnuaireComp extends HashMap{

	public AnnuaireComp(){
		
	}
	
	public void domaine(){
		for(int i=0; i<this.size();i++){
			System.out.println("nom :"+((Personne) this.get(i)).getn());
			System.out.println("prenom :"+((Personne) this.get(i)).getp());
		}
	}
	
	public Integer acces(String n, String p){
		boolean res=false;
		int x=0;
		for(int i=0; i<this.size();i++){
			if(((Personne) this.get(i)).getn().equals(n) && ((Personne) this.get(i)).getp().equals(p)){
				x=i;
				res=true;
			}
		}
		if(res==false){
			return (Integer) null;
		}else{
			return x;
		}
	}
	
	public void adjonction(String nom, String prenom, int x){
		if(this.acces(nom,prenom).equals(null)){
			this.put(x, new Personne(nom,prenom));
		}
	}
	
	public void suppression(String nom, String prenom){
		if(this.acces(nom,prenom)!=null){
			this.remove(this.acces(nom,prenom));
		}
	}
	
	public void changement(int x, String nom, String prenom){
		if(this.acces(nom,prenom)==null){
			this.suppression(nom,prenom);
			this.adjonction(nom, prenom, x);
		}
	}
}


