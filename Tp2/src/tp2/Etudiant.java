package tp2;

import java.util.ArrayList;
import java.util.HashMap;

public class Etudiant {

	private Identite id;
	private Formation form;
	private HashMap<String,ArrayList<Float>> resultats;
	
	public Etudiant(Identite i, Formation f){
		this.id=i;
		this.form=f;
		this.resultats=new HashMap<String,ArrayList<Float>>();
	}
	
	
	public void ajoutNote(String m, Float n){
		if((n<0 || n>20)&&this.form.getNom().equals(m)){
			
		}else{
			ArrayList<Float> notes= this.resultats.get(m);
			notes.add(n);
			this.resultats.put(m,notes);
		}
	}
	
	
}
