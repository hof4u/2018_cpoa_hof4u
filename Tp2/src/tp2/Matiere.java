package tp2;

import java.util.ArrayList;

public class Matiere {

	private String nom;
	private ArrayList<Integer> notes;
	private int coef;
	
	
	public Matiere (String n, int c){
		this.nom=n;
		this.coef=c;
		this.notes=new ArrayList<Integer>();
	}
	
	public void ajoutNote(int n){
		this.notes.add(n);
	}
	
	public void suprNotes(){
		this.notes.clear();
	}
	
	public String getNom(){
		return this.nom;
	}
	
	public int getCoef(){
		return this.coef;
	}
	
	public double getMoy(){
		double res=0;
		for(int i=0 ; i<this.notes.size();i++){
			res+=this.notes.get(i);
		}
		res=res/this.notes.size();
		return res;
	}
}
