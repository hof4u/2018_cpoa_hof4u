package tp2;

public class Identite {

	private String id;
	private String nom;
	private String prenom;
	
	public Identite(String id, String n, String p){
		this.id=id;
		this.nom=n;
		this.prenom=p;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
}
