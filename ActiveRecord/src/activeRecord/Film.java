package activeRecord;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Film {
	private String titre;
	private int id;
	private int id_real ;
	
	public Film(String titreFilm, Personne pers){
		titre=titreFilm;
		id_real=pers.getId();
		id=-1;
	}
	
	private Film(int filmId,int realId,String titreFilm){
		id=filmId;
		id_real=realId;
		titre=titreFilm;
	}
	
	public static Film findById(int id){
		
		String requet = "select * from film where id= ? ";
		Film film = null;
		
		try {
			
			PreparedStatement state = DBConnection.getInstance().getConnection().prepareStatement(requet);
			state.setInt(1, id);
			ResultSet resultS = state.executeQuery();
			
			if (resultS.next()) {
				
				Personne pers=Personne.findById(resultS.getInt("ID_REA"));
				film = new Film(resultS.getString("titre"), pers);
				film.id = resultS.getInt("id");
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return film;
	}
	
	
	public static List<Film> findByRealisateur(Personne pers) throws RealisateurAbsentException{
		
		List<Film> listFilm=new ArrayList<Film>();
		String requet = "select * from film where id_rea= ? ";
		
		if(pers.getId()==-1){
			throw new RealisateurAbsentException("la personne ne figure pas dans la table");
		}
		
		if(pers==null){
			throw new RealisateurAbsentException("la personne n'existe pas");
		}
		
		try {
			
			PreparedStatement state = DBConnection.getInstance().getConnection().prepareStatement(requet);
			state.setInt(1, pers.getId());
			ResultSet resultS = state.executeQuery();
			
			while(resultS.next()) {
				
				if(resultS.getInt("id_rea")==-1){
					throw new RealisateurAbsentException("la personne ne figure pas dans la table");
				}
				
				listFilm.add(new Film(resultS.getInt("id"),resultS.getInt("id_rea"),resultS.getString("titre")));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listFilm;
	}
	
	
	public static void createTable(){
		
		try {
			
			Statement state=DBConnection.getInstance().getConnection().createStatement();
			state.executeUpdate("CREATE TABLE IF NOT EXISTS `film` (  `ID` int(11) NOT NULL AUTO_INCREMENT,  `TITRE` varchar(40) NOT NULL,  `ID_REA` int(11) DEFAULT NULL,  PRIMARY KEY (`ID`),  KEY `ID_REA` (`ID_REA`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ");
			state.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void save() throws RealisateurAbsentException{
		
		if(id_real==-1){
			throw new RealisateurAbsentException("la personne choisi n'existe pas");
		}
		
		if(id==-1){
			
			try {
				
				PreparedStatement state=DBConnection.getInstance().getConnection().prepareStatement("INSERT INTO film ( TITRE, ID_REA) values ( ? , ? )");
				state.setInt(2, id_real);
				state.setString(1, titre);
				state.executeUpdate();
			
				state=DBConnection.getInstance().getConnection().prepareStatement("select id from film where titre= ? and id_rea= ? ");
				state.setInt(2, id_real);
				state.setString(1, titre);

				ResultSet resultS= state.executeQuery();
				resultS.next();
				id=resultS.getInt("id");
				resultS.close();
				state.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		else{
			
			try {
				
				PreparedStatement state=DBConnection.getInstance().getConnection().prepareStatement("update film set titre= ? , id_rea= ? where id= ? ");
				state.setString(1, titre);
				state.setInt(2, id_real);
				state.setInt(3, id);
				state.executeUpdate();	
				state.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	
	public static void deleteTable(){
		
		try {
			
			Statement state=DBConnection.getInstance().getConnection().createStatement();
			state.executeUpdate("drop table film");
			state.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Personne getRealisateur(){
		return Personne.findById(id_real);
	}

	public String getTitre() {
		return titre;
	}

	public int getId() {
		return id;
	}

	public int getId_real() {
		return id_real;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
}
