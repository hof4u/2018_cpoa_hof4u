package activeRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;



public class DBConnection {
	
	private static DBConnection instance;
	private Connection connection;
	private static  String pseudo = "root";
	private static  String Mdp = "";
	private static  String serveur = "localhost";
	private static  String port = "3306";
	private static String Nomdb = "testPersonne";
	
	public Connection getConnection(){
		return connection;
	}
	
	public static DBConnection getInstance(){
		if(instance==null){
			instance=new DBConnection();
		}
		return instance;
	}
	
	
	public static void setNomDB(String nomDB){
		Nomdb=nomDB;
		instance=new DBConnection();
	}
	
	
	public DBConnection(){
		Properties connectionProps = new Properties();
		String urlDB = "jdbc:mysql://" + serveur + ":";
		connectionProps.put("password", Mdp);
		connectionProps.put("user", pseudo);
		urlDB += port + "/" + Nomdb;
		try {
			connection = DriverManager.getConnection(urlDB, connectionProps);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
