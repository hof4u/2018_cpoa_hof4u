package activeRecord;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Personne {
	private int id;
	private String nom;
	private String prenom;

	public Personne(String n, String p) {
		nom = n;
		prenom = p;
		id = -1;
	}

	
	public static void createTable(){
		
		try {
			
			Statement state=DBConnection.getInstance().getConnection().createStatement();
			state.executeUpdate("CREATE TABLE IF NOT EXISTS `personne` (  `ID` int(11) NOT NULL AUTO_INCREMENT,  `NOM` varchar(40) NOT NULL,  `PRENOM` varchar(40) NOT NULL,  PRIMARY KEY (`ID`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ");
			state.close();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public static void deleteTable(){
		
		try {
			
			Statement state=DBConnection.getInstance().getConnection().createStatement();
			state.executeUpdate("drop table personne");
			state.close();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	
	public void delete(){
		
		try {
			
			PreparedStatement state=DBConnection.getInstance().getConnection().prepareStatement("delete from Personne where nom= ? and prenom= ? ");
			state.setString(1, this.nom);		
			state.setString(2, this.prenom);	
			state.executeUpdate();
			state.close();
			this.id=-1;
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public void save(){
		
		if(id==-1){
			
			try {
				
				PreparedStatement state=DBConnection.getInstance().getConnection().prepareStatement("insert into personne (nom, prenom)  values ( ? , ? )");
				state.setString(1, nom);
				state.setString(2, prenom);
				state.executeUpdate();
				state=DBConnection.getInstance().getConnection().prepareStatement("select id from personne where nom= ? and prenom= ? ");
				state.setString(1, nom);
				state.setString(2, prenom);
				ResultSet resultS= state.executeQuery();
				resultS.next();
				id=resultS.getInt("id");
				resultS.close();
				state.close();
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
		else{
			
			try {
				
				PreparedStatement state=DBConnection.getInstance().getConnection().prepareStatement("update personne set nom= ? , prenom= ? where id= ? ");
				state.setString(1, nom);
				state.setString(2, prenom);
				state.setInt(3, id);
				state.executeUpdate();	
				state.close();
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		
		
	}
	
	
	
	public static ArrayList<Personne> findAll() {
		
		ArrayList<Personne> list = new ArrayList<Personne>();
		
		try {
			
			Statement state = DBConnection.getInstance().getConnection().createStatement();
			String query = "select * from personne";
			ResultSet resultS = state.executeQuery(query);
			
			while (resultS.next()) {
				
				Personne pers = new Personne(resultS.getString("nom"), resultS.getString("prenom"));
				pers.id = resultS.getInt("id");
				list.add(pers);
			}

			resultS.close();
			state.close();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return list;
	}

	
	public static Personne findById(int id) {
		
		Personne pers = null;
		String requet = "select * from personne where id= ? ";
		
		try {
			
			PreparedStatement state = DBConnection.getInstance().getConnection().prepareStatement(requet);
			state.setInt(1, id);
			ResultSet resultS = state.executeQuery();
			
			if (resultS.next()) {
				
				pers = new Personne(resultS.getString("nom"), resultS.getString("prenom"));
				pers.id = resultS.getInt("id");
			}
			
		} catch (SQLException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pers;
	}

	
	public static ArrayList<Personne> findByName(String name) {
		
		ArrayList<Personne> list = new ArrayList<Personne>();
		
		try {
			
			String requet = "select * from personne where nom= ? ";
			PreparedStatement state = DBConnection.getInstance().getConnection().prepareStatement(requet);
			state.setString(1, name);
			ResultSet resultS = state.executeQuery();
			
			while (resultS.next()) {
				
				Personne pers = new Personne(resultS.getString("nom"), resultS.getString("prenom"));
				pers.id = resultS.getInt("id");
				list.add(pers);
			}

			resultS.close();
			state.close();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return list;
	}
	
	


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public int getId() {
		return id;
	}

}
