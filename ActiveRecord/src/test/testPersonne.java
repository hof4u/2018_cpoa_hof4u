package test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.*;

import activeRecord.Personne;

public class testPersonne {
	
	@Before
	public void initialisation(){
		
		Personne.createTable();
		
		Personne pers1 = new Personne("Scott","Ridley");
		Personne pers2 = new Personne("Fincher","David");
		Personne pers3 = new Personne("Spielberg","Steven");
		Personne pers4 = new Personne("Kubrick","Stanley");
		pers1.save();
		pers2.save();
		pers3.save();
		pers4.save();
	}
	
	@After
	public void suppression(){
		
		Personne.deleteTable();
	}
	
	@Test
	public void testFindAll() {
		
		List<Personne> listPers=Personne.findAll();
		
		assertTrue("La preimere personne n'a pas le bon nom",listPers.get(0).getNom().equals("Scott"));
		assertTrue("La deuxieme personne n'a pas le bon nom",listPers.get(1).getNom().equals("Fincher"));
		assertTrue("La troisieme personne n'a pas le bon nom",listPers.get(2).getNom().equals("Spielberg"));
		assertTrue("La quatrieme personne n'a pas le bon nom",listPers.get(3).getNom().equals("Kubrick"));
	}
	
	@Test
	public void testFindById(){
		
		Personne pres=Personne.findById(1);
		assertTrue("La personne doit �tre Ridley Scott", pres.getNom().equals("Scott"));
	}
	
	@Test
	public void testNonFonctionnelFindById(){
		
		Personne pers=Personne.findById(6);
		assertTrue("Le r�sultat attendu est null", pers==null);
	}
	
	@Test
	public void testFindByName(){
		
		List<Personne> listPers=Personne.findByName("Fincher");
		assertTrue("l'id doit �tre �gal � 2", listPers.get(0).getId()==2);
		assertTrue("La personne doit �tre David Fincher", listPers.get(0).getNom().equals("Fincher"));
	}
	
	@Test
	public void testNonFonctionnelFindByName(){
		
		List<Personne> listPers=Personne.findByName("");
		assertTrue("la liste devrait �tre vide", listPers.isEmpty());
	}
	
	@Test
	public void testDelete(){
		
		Personne pers=Personne.findById(1);
		pers.delete();
		List<Personne> listPers=Personne.findByName(pers.getNom());
		assertTrue("La liste devrait �tre vide", listPers.isEmpty());
		assertTrue("La personne ne devrait pas exister", pers.getId()==-1);
	}
	
	@Test
	public void testMiseAJour(){
		
		Personne p=Personne.findById(4);
		p.setNom("Robert");
		p.save();
		p=Personne.findById(1);
		assertTrue("La base de donn�es n'a pas pris en compte la modification", p.getNom().equals("Robert"));
	}
}
