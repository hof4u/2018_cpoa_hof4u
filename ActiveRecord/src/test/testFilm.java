package test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.*;

import activeRecord.Film;
import activeRecord.Personne;
import activeRecord.RealisateurAbsentException;

public class testFilm {

	@Before
	public void initialisation(){
		
		Personne.createTable();
		
		Personne pers1 = new Personne("Scott","Ridley");
		Personne pers2 = new Personne("Fincher","David");
		Personne pers3 = new Personne("Spielberg","Steven");
		Personne pers4 = new Personne("Kubrick","Stanley");
		pers1.save();
		pers2.save();
		pers3.save();
		pers4.save();
		
		Film.createTable();
		
		Film film1 = new Film("Alien",pers1);
		Film film2 = new Film("Blade Runner",pers1);
		Film film3 = new Film("Alien3",pers2);
		Film film4 = new Film("Fight Club",pers2);
		Film film5 = new Film("Arche Perdue",pers3);
		Film film6 = new Film("Temple Maudit",pers3);
		Film film7 = new Film("Orange Mecanique",pers4);
		
		try{
			
			film1.save();
			film2.save();
			film3.save();
			film4.save();
			film5.save();
			film6.save();
			film7.save();
		}
		
		catch(RealisateurAbsentException e){
			e.printStackTrace();
		}
	}
	
	@After
	public void suppression(){
		
		Film.deleteTable();
		Personne.deleteTable();
	}
	
	@Test
	public void testFindById() {
		
		Film film = Film.findById(1);
		assertTrue("Le film selectionn� n'est pas celui attendue", film.getTitre().equals("Alien"));
		assertTrue("le realisateur du film selectionn� n'est pas celui attendue",
				Personne.findById(film.getId()).getId() == film.getRealisateur().getId());
	}

	@Test
	public void testFindByIdNonFonctionnel() {
		Film film = Film.findById(-1);
		assertTrue("le film attendu doit �tre null", film == null);
	}

	@Test
	public void testFindByRealisateur() {
		Personne pers=Personne.findById(1);
		List<Film> listFilm=null;
		try {
			listFilm = Film.findByRealisateur(pers);
		} catch (RealisateurAbsentException e) {
			e.printStackTrace();
		}
		assertTrue("les films demand�s ne sont pas ceux attendue", listFilm.get(0).getTitre().equals("Alien"));
		assertTrue("les films demand�s ne sont pas ceux attendue", listFilm.get(1).getTitre().equals("Blade Runner"));
		
	}
}
