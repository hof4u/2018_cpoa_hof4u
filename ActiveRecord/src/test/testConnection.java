package test;

import static org.junit.Assert.*;

import java.sql.Connection;

import org.junit.Test;

import activeRecord.DBConnection;

public class testConnection {

	@Test
	public void testNonNull(){
		
		DBConnection d = DBConnection.getInstance();
		Connection c= DBConnection.getInstance().getConnection();
		DBConnection d2 = DBConnection.getInstance();
		Connection c2 = DBConnection.getInstance().getConnection();
		assertTrue("Il y a diff�rente instance identique",d==d2);
		assertTrue("Il y a plusieurs connexion",c==c2);
	}
	
	@Test
	public void testSetNom(){
		Connection c= DBConnection.getInstance().getConnection();
		DBConnection d = DBConnection.getInstance();
		DBConnection.setNomDB("testBdNom");
		DBConnection d2 = DBConnection.getInstance();
		Connection c2 = DBConnection.getInstance().getConnection();
		assertTrue("Les connexions doivent �tre diff�rentes",c!=c2);
		assertTrue("Les instances doivent �tre diff�rentes",d!=d2);
	}
}
