package ex1;


public class FabriqueIntersection {
	
	private double probaVoiture;
	private double probaBus;
	private double probaByc;
	private double probaPieton;
	
	public FabriqueIntersection(){
		this.probaVoiture=0.8;
		this.probaBus=0.05;
		this.probaByc=0.05;
		this.probaPieton=0.1;
	}
	
	public FabriqueIntersection(double voit, double bus, double byc, double piet){
		this.probaVoiture=voit;
		this.probaBus=bus;
		this.probaByc=byc;
		this.probaPieton=piet;
	}
	
	public Vehicule creerVehicule(){
		double rand= Math.random();
		Vehicule v =null;
		if(rand<0.8){
			v= new Voiture();
		}else{
			if(rand<0.9){
				v= new Pieton();
			}else{
				if(rand<0.95){
					v= new Bus();
				}else{
					v=new Bicyclette();
				}
			}
		}
		return v;
	}
}
