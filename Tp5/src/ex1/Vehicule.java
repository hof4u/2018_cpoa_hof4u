package ex1;


public abstract class Vehicule{

	protected double vitesse;
	protected double vitesseMax;
	protected String type;
		
	protected Vehicule(double v, String t){
		this.vitesseMax=v;
		this.type=t;
	}
	
	public double getVitesse(){
		return this.vitesse;
	}
	
	public String getType(){
		return this.type;
	}
	
	public void accelerer(double a){
		this.vitesse=+a;
	}
	
	public void decelerer(double d){
		this.vitesse=-d;
	}
	
}
