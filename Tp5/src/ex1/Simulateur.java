package ex1;

import java.util.HashMap;

public class Simulateur {

	private FabriqueIntersection fi;
	
	public Simulateur(){
		this.fi = new FabriqueIntersection();
	}
	
	public HashMap genrerStats(){
		HashMap tab  = new HashMap();
		tab.put("Voiture", 0);
		tab.put("Bus", 0);
		tab.put("Bicyclette", 0);
		tab.put("Pieton", 0);
		Vehicule v;
		for(int i=0;i<100;i++){
			v=fi.creerVehicule();
			if(v.getType().equals("Voiture")){
				tab.put("Voiture", (int)(tab.get("Voiture"))+1);
			}
			if(v.getType().equals("Bus")){
				tab.put("Bus", (int)(tab.get("Bus"))+1);
			}
			if(v.getType().equals("Bicyclette")){
				tab.put("Bicyclette", (int)(tab.get("Bicyclette"))+1);
			}
			if(v.getType().equals("Pieton")){
				tab.put("Pieton", (int)(tab.get("Pieton"))+1);
			}
		}
		return tab;
	}
}
