package ex2;

public class Aeroport {
	
	public static Aeroport instance;
	private boolean piste_libre;
	
	public Aeroport(){
		this.piste_libre=true;
	}
	
	public static synchronized Aeroport getInstance(){
		if(instance==null){
			instance=new Aeroport();
			
		}
		return instance;
	}
	
	public synchronized boolean autoriserAdecoller(){
		boolean res=false;
		if(this.piste_libre==true){
			res=true;
			this.piste_libre=false;
		}
		return res;
		
	}
	
	public synchronized boolean liberer_piste(){
		this.piste_libre=true;
		return piste_libre;
		
	}
	
}
