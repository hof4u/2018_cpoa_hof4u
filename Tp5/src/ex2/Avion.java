package ex2;

class Avion extends Thread {
	private String nom;
	private Aeroport a;
	
	public Avion(String s){
		this.nom=s;
	}

	
	public void run(){
		this.a = Aeroport.getInstance();
		System.out.println("Je suis avion " + this.nom + " sur aeroport " + this.a);
		synchronized (a) {
			
			this.a.autoriserAdecoller();
			
			long tempsDecollage = (long) (Math.random()*10000);
			System.out.println(this.nom+" decolle pendant "+tempsDecollage/1000.0+" secondes");
			try {
				Avion.sleep((tempsDecollage));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(this.nom+" libere la piste");
			a.liberer_piste();
		}
	}
	
}